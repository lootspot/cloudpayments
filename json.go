package cloudpayments

// Status is used for cloudpayment default
// json error unmarshaling
type Status struct {
	Success bool   `json:"Success"`
	Message string `json:"Message"`
}

// Payment cock
type Payment struct {
	Amount               float64 `json:"Amount"`
	Currency             string  `json:"Currency"`
	IPAddress            string  `json:"IpAddress"`
	Name                 string  `json:"Name"`
	CardCryptogramPacket string  `json:"CardCryptogramPacket"`
	InvoiceID            string  `json:"InvoiceId,omitempty"`
	Description          string  `json:"description,omitempty"`
	AccountID            string  `json:"AccountId,omitempty"`
	Email                string  `json:"email,omitempty"`
}

// Model used for json unmarshaling
type Model struct {
	PublicID              string `json:"PublicId"`
	TransactionID         int    `json:"TransactionId"`
	Amount                int
	Currency              string
	PaymentAmount         int
	PaymentCurrency       string
	AccountID             string `json:"AccountId"`
	Email                 string
	Descriptio            string
	PayoutAmount          int
	AuthCode              int
	TestMode              bool
	Rrn                   string
	OriginalTransactionID int    `json:"OriginalTransactionId"`
	IPAddress             string `json:"IpAddress"`
	IPCountry             string `json:"IpCountry"`
	IPCity                string `json:"IpCity"`
	Status                string
	StatusCode            int
	Reason                string
	ReasonCode            int
	CardHolderMessage     string
	Type                  int
	Refunded              bool
	Name                  string
	SubscriptionID        int `json:"SubscriptionId"`
	GatewayName           string
}
