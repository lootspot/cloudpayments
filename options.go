package cloudpayments

// Options stored api request options
type Options struct {
	RequestID string
}

// Option sets options
type Option func(*Options)

// RequestID sets RequestID in options
func RequestID(id string) Option {
	return func(ops *Options) {
		ops.RequestID = id
	}
}

func updateOptions(options *Options, args []Option) {
	for _, option := range args {
		option(options)
	}
}
