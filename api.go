package cloudpayments

import (
	"bytes"
	"encoding/base64"
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
)

const (
	basic   = "Basic"
	baseURL = "https://api.cloudpayments.ru/"
)

// New creates a new API instance
// PublicID is used for username and
// API secret is used for password.
// both can be optained in your
// cloudpayments personal cabinet
// if idFac no specified uses TimeIdFactory
func New(username, password string, idFac ...IdempotencyFactory) *API {
	api := &API{
		Username: username,
		Password: password,
	}

	if idFac != nil {
		api.IDFactory = idFac[0]
	} else {
		api.IDFactory = TimeIDFactory{}
	}

	return api
}

// API stores authenfication data
type API struct {
	Username  string
	Password  string
	IDFactory IdempotencyFactory
}

// Test is wrapper for https://api.cloudpayments.ru/test
func (api *API) Test() (status Status, err error) {
	request, err := http.NewRequest("POST", baseURL+"test", bytes.NewBuffer([]byte{}))
	if err != nil {
		return Status{}, err
	}

	request.Header.Set("Content-Type", "application/json")
	request.Header.Set("Authorization", api.authHeader())
	request.Header.Set("X-Request-ID", api.IDFactory.ID())

	client := &http.Client{}
	response, err := client.Do(request)
	if err != nil {
		return Status{}, err
	}
	defer response.Body.Close()

	if response.StatusCode == 401 {
		return Status{}, errors.New(response.Status)
	}

	if err := json.NewDecoder(response.Body).Decode(&status); err != nil {
		return Status{}, err
	}

	return status, nil
}

// CryptogramPayment pay by cryptogram
func (api API) CryptogramPayment(requestData Payment) (status Status, err error) {
	data, err := json.Marshal(requestData)
	if err != nil {
		return status, err
	}

	request, err := http.NewRequest("POST", baseURL+"test", bytes.NewBuffer(data))
	if err != nil {
		return status, err
	}

	request.Header.Set("Content-Type", "application/json")
	request.Header.Set("Authorization", api.authHeader())
	request.Header.Set("X-Request-ID", api.IDFactory.ID())

	client := &http.Client{}
	response, err := client.Do(request)
	if err != nil {
		return Status{}, err
	}
	defer response.Body.Close()
	if response.StatusCode == 401 {
		return Status{}, errors.New(response.Status)
	}

	if err := json.NewDecoder(response.Body).Decode(&status); err != nil {
		return Status{}, err
	}

	return status, nil
}

// Process3DSecure wraps https://api.cloudpayments.ru/payments/cards/post3ds
func (api API) Process3DSecure(transactionID int, paRes string) (status Status, err error) {
	data, err := json.Marshal(
		struct {
			TransactionID int    `json:"TransactionId"`
			PaRes         string `json:"PaRes"`
		}{
			TransactionID: transactionID,
			PaRes:         paRes,
		},
	)
	if err != nil {
		return Status{}, err
	}

	request, err := http.NewRequest("POST", baseURL+"payments/refund", bytes.NewBuffer(data))
	if err != nil {
		return Status{}, err
	}

	request.Header.Set("Content-Type", "application/json")
	request.Header.Set("Authorization", api.authHeader())
	request.Header.Set("X-Request-ID", api.IDFactory.ID())

	client := &http.Client{}
	response, err := client.Do(request)
	if err != nil {
		return Status{}, err
	}
	defer response.Body.Close()
	if response.StatusCode == 401 {
		return Status{}, errors.New(response.Status)
	}

	if err := json.NewDecoder(response.Body).Decode(&status); err != nil {
		return Status{}, err
	}

	return status, nil

}

// Refund wraps https://api.cloudpayments.ru/payments/refund
func (api API) Refund(transactionID int, amount float64) (status Status, err error) {
	data, err := json.Marshal(
		struct {
			TransactionID int     `json:"TransactionId"`
			Amount        float64 `json:"Amount"`
		}{
			TransactionID: transactionID,
			Amount:        amount,
		},
	)
	if err != nil {
		return Status{}, err
	}

	request, err := http.NewRequest("POST", baseURL+"payments/refund", bytes.NewBuffer(data))
	if err != nil {
		return Status{}, err
	}

	request.Header.Set("Content-Type", "application/json")
	request.Header.Set("Authorization", api.authHeader())
	request.Header.Set("X-Request-ID", api.IDFactory.ID())

	client := &http.Client{}
	response, err := client.Do(request)
	if err != nil {
		return Status{}, err
	}
	defer response.Body.Close()
	if response.StatusCode == 401 {
		return Status{}, errors.New(response.Status)
	}

	if err := json.NewDecoder(response.Body).Decode(&status); err != nil {
		return Status{}, err
	}

	return status, nil
}

// AuthHeader creates authenfication header from username and password
func (api API) authHeader() string {
	return "Basic " + base64.StdEncoding.EncodeToString(
		[]byte(
			fmt.Sprintf("%s:%s", api.Username, api.Password),
		),
	)
}
