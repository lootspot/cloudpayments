package cloudpayments

import "time"

// IdempotencyFactory creates request IDs
type IdempotencyFactory interface {
	ID() string
}

// TimeIDFactory uses unix timestamps as id's
type TimeIDFactory struct {
}

// ID returns unix time as id
func (tid TimeIDFactory) ID() string {
	return string(time.Now().Unix())
}
